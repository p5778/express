var express = require('express');
var router = express.Router();
var controller = require('../controller/books.controller');
//var authenticate = require('../middlewares/authenticate.middleware')

/* GET books listing. */
router.get('/', 
    //authenticate.isUser,
    controller.getAll,
);
//router.get('/', controller.addOne);
//router.get('/:id', controller.uppdateOne);
//router.get('/:id', controller.deleteOne);

module.exports = router;